#!/usr/bin/bash
# Seth Kenlon seth@opensource.com

# GNU All-Permissive License
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

CURL="aria2c -x4"
VERBOSE=0
CACHE="$HOME/.cache/pip/index.html"

function die() {
    echo $1
    exit
}

function parser() {
    [ $VERBOSE -gt 0 ] && echo "Converting file..."
    cat $CACHE | \
	awk --field-separator ">" '/a href/ {print $2 ;}' | \
	sed -e 's_</a__' \
	    > $HOME/.cache/pip/pip.list

    [ $VERBOSE -gt 0 ] && echo "Removing temp files if any exist..."
    [ -e $CACHE ] && rm $CACHE
}

function getter() {
    [ -d $HOME/.cache/pip ] || mkdir -p $HOME/.cache/pip/
    $CURL https://pypi.org/simple/ \
	  --dir $HOME/.cache/pip/ \
	  --out index.html || die "Update failed"
    parser
}

function lister() {
    [ $VERBOSE -gt 0 ] && echo "lister"

    if [ -e $HOME/.cache/pip/pip.list ]; then
	[ $VERBOSE -gt 0 ] && echo "found local pip.list file"
	cat $HOME/.cache/pip/pip.list
    else
	die "You must perform `pipget --get` first"
    fi
}

function searcher() {
    [ -e $HOME/.cache/pip/pip.list ] || getter
    grep -i "${ARG}" "$HOME"/.cache/pip/pip.list
    [ $VERBOSE -gt 0 ] && echo "Results for " "${ARG}"
}

function helper() {
    echo " "
    echo "$0 [--get|--list|--search <foo>]"
    echo " "
}

## parse opts
while [ True ]; do
if [ "$1" = "--help" -o "$1" = "-h" ]; then
    helper
    exit
elif [ "$1" = "--get" -o "$1" = "-g" -o "$1" = "--update" ]; then
    getter
    shift 1
elif [ "$1" = "--list" -o "$1" = "-l" ]; then
    lister
    shift 1
elif [ "$1" = "--search" -o "$1" = "-s" -o "$1" = "--grep" ]; then
    SEARCH=1
    ARG="$2"
    shift 2
elif [ "$1" = "--local-file" ]; then #this is for debugging
    parser
    CACHE="$2"
    shift 2    
elif [ "$1" = "--verbose" -o "$1" = "-v" ]; then
    VERBOSE=1
    shift 1
else
    break
fi
done

[ "x${INPUT}" = "x" ] && helper

[ "${SEARCH}" = "1" ] && searcher

exit 0
